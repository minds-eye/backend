#!/bin/sh

echo "Running startup.sh"
./wait-for-it.sh postgres:5432

echo "Creating Database"
npx sequelize-cli db:create

echo "Generating Idea Model"
npx sequelize-cli model:generate --name Idea --attributes idea:string

echo "Running Migrations"
npx sequelize-cli db:migrate

echo "Generating seeds"
npx sequelize-cli seed:generate --name demo-idea

echo "Running seeds"
npx sequelize-cli db:seed:all

echo "Starting server"
npx nodemon app
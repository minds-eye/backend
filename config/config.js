const { POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_DB } = process.env

module.exports = {
	development: {
		username: POSTGRES_USER,
		password: POSTGRES_PASSWORD,
		database: POSTGRES_DB,
		host: "postgres",
		dialect: "postgres"
	}
}

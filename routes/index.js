const Router = require("koa-router")

const { Idea } = require("../models/index")

const router = new Router({
	prefix: "/api"
})

// GET /api
// Get all ideas
router.get("/", async ctx => {
	try {
		const ideas = await Idea.findAll()
		ctx.status = 200
		ctx.body = ideas
	} catch (err) {
		console.log("An error occured in GET /api route", err)
		ctx.status = 500
		ctx.body = "Error trying to get ideas"
	}
})

// PUT /api
// CREATE idea
router.put("/", async ctx => {
	try {
		const { idea } = ctx.request.body
		const createdIdea = await Idea.create({ idea })
		ctx.status = 200
		ctx.body = "PUT /api"
	} catch (err) {
		console.log("An error occured in PUT /api route", err)
		ctx.status = 500
		ctx.body = "Error trying to create idea"
	}
})

// POST /api
// UPDATE idea
router.post("/", async ctx => {
	try {
		const { id, idea } = ctx.request.body
		const updateIdea = await Idea.update({ id, idea }, { where: { id } })
		ctx.status = 200
		ctx.body = "POST /api"
	} catch (err) {
		console.log("An error occured in POST /api route")
		ctx.status = 500
		ctx.body = "Error trying to update idea"
	}
})

// DELETE /api
// DELETE an idea
router.delete("/:id", async ctx => {
	try {
		const { id } = ctx.params
		const deletedIdea = await Idea.destroy({ where: { id } })
		ctx.status = 200
		ctx.body = "DELETE /api/:id"
	} catch (err) {
		console.log("An error occured in DELETE /api/:id route", err)
		ctx.status = 500
		ctx.body = "Error trying to delete idea"
	}
})

module.exports = router
